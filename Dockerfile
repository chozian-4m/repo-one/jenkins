ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/openjdk/openjdk8-devel
ARG BASE_TAG=1.8.0

FROM jenkins/jenkins:2.344-jdk8 as jenkins

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}


USER root

ENV JAVA_HOME /usr/lib/jvm/java
ENV JENKINS_UC https://updates.jenkins.io

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ARG http_port=8080
ARG agent_port=50000
ARG JENKINS_HOME=/var/jenkins_home
ARG REF=/usr/share/jenkins/ref

ENV JENKINS_HOME $JENKINS_HOME
ENV JENKINS_SLAVE_AGENT_PORT ${agent_port}
ENV REF $REF

COPY --from=jenkins /usr/local/bin/git-lfs /usr/local/bin/git-lfs

# Jenkins is run with user `jenkins`, uid = 1000
# If you bind mount a volume from the host or a data container,
# ensure you use the same uid
RUN mkdir -p $JENKINS_HOME && \
    chown ${uid}:${gid} $JENKINS_HOME && \
    groupadd -g ${gid} ${group} && \
    useradd -d "$JENKINS_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user} && \
    dnf update -y && dnf -y upgrade && \
    dnf install -y fontconfig git unzip --setopt=tsflags=nodocs && \
    git lfs install && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
# $REF (defaults to `/usr/share/jenkins/ref/`) contains all reference configuration we want
# to set on a fresh new installation. Use it to bundle additional plugins
# or config file with your custom jenkins Docker image.
    mkdir -p ${REF}/init.groovy.d && \
    chown -R ${user} "$JENKINS_HOME" "$REF"
    # chmod 00775 /usr/libexec/openssh/ssh-keysign

# Jenkins home directory is a volume, so configuration and build history
# can be persisted and survive image upgrades
VOLUME $JENKINS_HOME

COPY --from=jenkins /usr/share/jenkins/jenkins.war /usr/share/jenkins/jenkins.war
COPY --from=jenkins /usr/local/bin/jenkins-support /usr/local/bin/jenkins-support
COPY --from=jenkins /bin/tini /bin/tini
COPY --from=jenkins /sbin/tini /sbin/tini

COPY scripts/jenkins.sh /usr/local/bin/jenkins.sh
COPY scripts/plugins.sh /usr/local/bin/plugins.sh
COPY scripts/install-plugins.sh /usr/local/bin/install-plugins.sh
COPY scripts/jenkins-plugin-cli.sh /bin/jenkins-plugin-cli

COPY jenkins-plugin-manager.jar /opt

RUN chmod go-w \
        /usr/share/jenkins/jenkins.war \
        /usr/local/bin/jenkins-support \
        /bin/tini \
        /sbin/tini \
        /usr/local/bin/jenkins.sh \
        /usr/local/bin/plugins.sh \
        /usr/local/bin/install-plugins.sh \
        /bin/jenkins-plugin-cli \
        /opt/jenkins-plugin-manager.jar




# for main web interface:
EXPOSE ${http_port}

# will be used by attached slave agents:
EXPOSE ${agent_port}

ENV COPY_REFERENCE_FILE_LOG $JENKINS_HOME/copy_reference_file.log

USER ${user}

HEALTHCHECK --interval=10s --timeout=5s --start-period=1m --retries=5 CMD curl -I -f --max-time 5 http://localhost:8080/login?from=%2F || exit 1

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/jenkins.sh"]

# from a derived Dockerfile, can use `RUN plugins.sh active.txt` to setup $REF/plugins from a support bundle
